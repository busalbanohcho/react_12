import styles from './TesComponent.styles';
import styles from  './TesComponent.module.scss';
import styled from './styled-components';
import Button from '@mui/material/Button';

const MyStyledDiv = styled.h1`

color: red;
font-size: 25px;

&:hover: {
color: blue;
}
`

function TestComponent () { 
return (
<div>
    <div className={styles.textStyle('title')}>Стилизация компонентов</div>
    <div style={styles.divStylePlus('blue')}>Стилизация компонентов</div>
    <div style={styles.text}>Стилизация компонентов</div>
    <MyStyledDiv></MyStyledDiv>
    <Button variant="contained" color="success">КНОПКА</Button>
    </div>
)



}

export default TestComponent;